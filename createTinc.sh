#/bin/bash
# Script to manage easy the creation and configuration of a tinc client
# From: https://gitlab.com/selankon/tinc-utils

# Destination
DEST="/etc/tinc/"
STARTINCATEND=true #Start tinc daemon at end

# Net and host names
NETNAME=lolipop
HOSTNAME=host_$(hostname)
AUTOSTART=true # Put the autostart on system reboot

# Server settings
SERVERNAME=server # Let this string empty if you are creating the server (with a public ip address)
PUBLICADDRESS=ageof.lol
PORT= # Empty for 655 default port

# Configs
INTERFACE=tinc
MODE=switch
IP=192.168.25.101
SUBNET=$IP/32

# Redirect-gateway mode
# Used to redirect all trafic of your computer to tinc interface when server is up
REDIRECTGATEWAY= # Empty for not do it. Write the rules for the server up-down to redirect all traffic to him.
VPN_GATEWAY=10.1.0.1 # If you leave empty REDIRECTGATEWAY this don't do anything

######## BACKUP SSH SETTINGS ########
# Enable/Disable ssh backup
SSH=false
# PublicKey
SSHKEYFILE= # Use specific keyfile in all scp transaction. If empty just use .ssh/config configuration or user/pass
SshPublicKeyDest="/etc/tinc/$NETNAME/hosts" # Where to store the public key on the destination of scp
SshPrivateDest="/root/" # Where to store al /etc/tinc/netname directory with private key on scp destination

# Arrays that stores destinations to copy private or public keys
# Set to empty to avoid the Copy
# Fill it without comma separation
# [ip]=user [ip]=user
#
# Array that stores where to copy via scp your public key
declare -A publicKeyCopy=(
  # [${PUBLICADDRESS}]=root
)
# Array that stores where to copy via scp your private keys
declare -A privateKeyCopy=(
  # [192.168.1.100]=root [192.168.1.102]=root
)

##### Start Script #####

# Checking root
if [[ $EUID -ne 0 ]]
  then echo "Please run as root"
  exit
fi

# Checking for dependencies
tincBin="/usr/sbin/tincd"
echo "Checking for dependencies ($tincBin, $gitBin)"
# Install tinc
if [ ! -f  /usr/sbin/tincd ]; then
  apt install -y tinc
fi
# Install git
if [ ! -f  /usr/bin/git ]; then
  apt install -y git
fi

# Create all files
echo "Creating files.."
rewrite=true
if [[ -d $DEST$NETNAME ]]; then
  echo "Directory $DEST$NETNAME exists. "
  read -p "Do you want to re-write it?" yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) rewrite = false; break;;
        * ) echo "Please answer Yy or Nn.";;
    esac
fi
if $rewrite; then
  mkdir $DEST$NETNAME
  mkdir $DEST$NETNAME/hosts
  touch $DEST$NETNAME/tinc-up
  touch $DEST$NETNAME/tinc-down
  touch $DEST$NETNAME/tinc.conf
  touch $DEST$NETNAME/hosts/$HOSTNAME

  # fill tinc.conf
  echo "Writting tinc.conf"
  tinconf="$DEST$NETNAME/tinc.conf"
  echo "Name = $HOSTNAME" >> $tinconf
  echo "AddressFamily = ipv4"  >> $tinconf
  echo "Interface = $INTERFACE"  >> $tinconf
  if [ -n "$SERVERNAME" ]; then
      echo "ConnectTo = $SERVERNAME" >> $tinconf
  fi
  echo "Mode=$MODE"  >> $tinconf


  # Writting tinc-up
  echo "Writting tinc-up"
  tincup="$DEST$NETNAME/tinc-up"
  chmod 755 $tincup
  echo "ip link set \$INTERFACE up" >> $tincup
  echo "ip addr add $IP/24 dev \$INTERFACE" >> $tincup
  # Deprecated
  #echo "ifconfig \$INTERFACE $IP netmask 255.255.255.0" >> tinc-up

  # Writting tinc-down
  echo "Writting tinc-down"
  tincdown="$DEST$NETNAME/tinc-down"
  chmod 755 $tincdown
  echo "ip addr $IP/24 dev \$INTERFACE " >> $tincdown
  echo "ip link set \$INTERFACE down" >> $tincdown
  # Deprecated
  #echo "ifconfig \$INTERFACE down" >> tinc-down

  # Writting host file
  echo "Writting host file"
  tinchosts="$DEST$NETNAME/hosts"
  echo "Subnet = $SUBNET" >> $tinchosts/$HOSTNAME
  if [ ! -n "$SERVERNAME" ]; then # Add the redirect-gateway for the server
      echo "Subnet = 0.0.0.0/0" >> $tinchosts/$HOSTNAME
      echo "Address = $PUBLICADDRESS" >> $tinchosts/$HOSTNAME
  else
  	touch $tinchosts/$SERVERNAME
  	echo "<< IMPORTANT!! >> Manually copy or other peers server hosts file!"
  	#echo -e $SERVERKEY >> hosts/$SERVERNAME
  fi
  if [ -n "$PORT" ]; then
  	echo "Port = $PORT" >> $tinchosts/$HOSTNAME
  fi

  if $AUTOSTART ; then
    echo $NETNAME >> ${DEST}nets.boot
  fi

  # Creating keys
  echo "Creating keys..."
  sudo tincd -n $NETNAME -K4096 -c $DEST$NETNAME

  # Creating redirect gateway files
  if [ -n "$REDIRECTGATEWAY" ]; then
  	echo "Creating redirect gateway files"

  	SERVERUP=$tinchosts/$SERVERNAME-up
  	echo "Creating $SERVERUP"
  	touch $SERVERUP
  	chmod 755 $SERVERUP
  	echo "#!/bin/sh" >> $SERVERUP
  	echo "VPN_GATEWAY=$VPN_GATEWAY" >> $SERVERUP
  	echo "ORIGINAL_GATEWAY=\`ip route show | grep ^default | cut -d ' ' -f 2-5\`" >> $SERVERUP
  	echo "ip route add \$REMOTEADDRESS \$ORIGINAL_GATEWAY" >> $SERVERUP
  	echo "ip route add \$VPN_GATEWAY dev \$INTERFACE" >> $SERVERUP
  	echo "ip route add 0.0.0.0/1 via \$VPN_GATEWAY dev \$INTERFACE" >> $SERVERUP
  	echo "ip route add 128.0.0.0/1 via \$VPN_GATEWAY dev \$INTERFACE" >> $SERVERUP

  	SERVERDOWN=$tinchosts/$SERVERNAME-down
  	echo "Creating hosts/$SERVERDOWN"
  	touch $SERVERDOWN
  	chmod 755 $SERVERDOWN
  	echo "#!/bin/sh" >> $SERVERDOWN
  	echo "ORIGINAL_GATEWAY=\`ip route show | grep ^default | cut -d ' ' -f 2-5\`" >> $SERVERDOWN
  	echo "ip route del \$REMOTEADDRESS \$ORIGINAL_GATEWAY" >> $SERVERDOWN
  	echo "ip route del \$VPN_GATEWAY dev \$INTERFACE" >> $SERVERDOWN
  	echo "ip route del 0.0.0.0/1 dev \$INTERFACE" >> $SERVERDOWN
  	echo "ip route del 128.0.0.0/1 dev \$INTERFACE " >> $SERVERDOWN
  fi
fi

#### SSH BACKUP ####
# Function that iterates throught an array of [ip]=user values
# Try to scp this values to copy the private or public key
# $1 -> Associative array with relation of ip and user
# $2 -> Key destination
# $3 -> Key Origin
scpArray (){
  eval "declare -A _array="${1#*=}
  dest=$2
  orig=$3
  for i in "${!_array[@]}"
  do
      ip_address=$i
      user=${_array[$i]}

      echo "- Trying to copy $orig directory key on to $user@$ip_address:$dest"
      options="-rp" # Recursive, not modify creation dates
      # If is a password file
      if [[ -f $SSHKEYFILE ]]; then
        echo "Setting keyfile"
        options+=" -i $SSHKEYFILE"
      fi
      # Important to execute it as sudo in order to take the private key properly
      SSH="sudo scp $options $orig $user@$ip_address:$dest"
      echo "$SSH"
      eval $SSH
      if [[ $? != 0 ]]; then
      	echo "ERROR: SCP error on $user@$PUBLICADDRESS"
      fi
  done
}


if $SSH; then
  echo "* Starting public key scp backup"

  if [ ${#publicKeyCopy[@]} -eq 0 ]; then
    echo "- No public key backup configured"
  else
    scpArray "$(declare -p publicKeyCopy)" $SshPublicKeyDest  ${DEST}${NETNAME}/hosts/$HOSTNAME
  fi

  if [ ${#privateKeyCopy[@]} -eq 0 ]; then
    echo "- No private key backup configured"
  else
    scpArray "$(declare -p privateKeyCopy)" $SshPrivateDest  ${DEST}${NETNAME}
  fi
fi

if $STARTINCATEND ; then
  echo "----------------------"
  echo "Strating tinc"

  tincd -n $NETNAME
  if [[ $? != 0 ]]; then
    echo "!ERROR: starting tincd"
  fi
fi

echo "done"
echo "If needed, copy your server or other hosts files to connect to other peers on:"
echo "$DEST$NETNAME/hosts/"

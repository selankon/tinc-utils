#!/bin/bash
# Backup all tinc directory to somewhere into a tar.gz
# $1 is the owner of the tar.gz file

TINCDIR="/etc/tinc"
HOSTNAME="myhostname"
DESTFILE="/backupdir"

OWNER=$(whoami)
if [[ ! -z $1 ]]; then
  OWNER=$1
fi

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exec sudo -- "$0" "$(whoami)"
fi

if [[ -d $TINCDIR ]]; then
  if [[ ! -d $(dirname $DESTFILE) ]]; then
    echo "Directory not exists $(dirname $DESTFILE)"
    mkdir $(dirname $DESTFILE)
  fi
  tar -czvf $DESTFILE $TINCDIR

  if [[ ! -z OWNER ]]; then
    chown $OWNER:$OWNER $DESTFILE
    chmod 700 $DESTFILE
  fi

fi

#!/bin/bash
# Script that authomatize to copy a tinc public key into a git repository.
# After that commit and push.
# Then make a symbolic link of the git key repository onto the hosts filder on the network.
# In this way, each time that you update with pull the git repo you will get all the public keys on the repo.

# Destination
DEST="/etc/tinc/"
NETNAME=lanparty
HOSTNAME=host_$(whoami)  # this is the hostname that wants to add to the git repo

ONLYLINK=false # dont try to clone or pull the repository, just make the ln -s of  ln -s ${GITDEST}${GITKEYFOLDER} hosts

######## BACKUP GIT SETTINGS ########
# Enable/Disable git backup
GIT=true
GITREPO="https://gitlab.com/repoUrl" # Url of the repo
GITDEST="/root/git/repo/"  # Where to clone the repo
GITKEYFOLDER="." # Folder where to store the key inside the git repo.
GITPULL=false # Used to pull changes if the repo already exists. By default false

GITINSTALLAUTOPULL=true # Install autopull script daemon


gitBin="/usr/bin/git"
# Install git
if [ ! -f  $gitBin ]; then
  echo "Git not found on $gitBin, proceding to install"
  apt install -y git
fi

# Test that git is configured
if $GIT; then
  if [[ -z $(git config --global user.name) || -z $(git config --global user.email) ]]; then
    echo "!ERROR: you must to configure git:"
    echo 'git config --global user.email "you@example.com"'
    echo 'git config --global user.name "Your Name"'
    exit
  fi
fi

#### GIT BACKUP ####
# Create git directory
if $GIT; then
  echo "----------------------"
  echo "* Starting git backup"

  mkdir -p $GITDEST
  # If repo doesn't exist clone it. If exists pull it
  if [ ! -d "$GITDEST/.git" ]
  then
      echo "[X] Repo doesn't exist. Cloning it"
      git -C $GITDEST clone $GITREPO $GITDEST
      if [[ $? != 0 ]]; then
        echo "!ERROR: cloning git"
      fi
  else
      echo "[X] Repo already exists. Pulling changes :" $GITPULL
      if $GITPULL; then
        git -C $GITDEST pull $GITREPO
        if [[ $? != 0 ]]; then
          echo "!ERROR: pulling git"
        fi
      fi
  fi

  # Copy the key to the repo and push it
  echo "* Copying public key to the repo"
  mkdir -p ${GITDEST}${GITKEYFOLDER}
  cp ${DEST}${NETNAME}/hosts/$HOSTNAME ${GITDEST}${GITKEYFOLDER}
  if [[ $? != 0 ]]; then
    echo "!ERROR: copying public key"
  else
    echo "Copied"
  fi
  # Pushing the changes
  echo "* Pushing new key ${GITKEYFOLDER}/${HOSTNAME}"
  git -C $GITDEST add ${GITKEYFOLDER}"/"${HOSTNAME}
  git -C $GITDEST commit -m "Added key of $HOSTNAME"
  if [[ $? != 0 ]]; then
    echo "!ERROR: commiting "
  fi
  git -C $GITDEST push origin master
  if [[ $? != 0 ]]; then
    echo "!ERROR: Error pushing "
  fi

  echo "* Creating symbolic link from ${GITDEST}${GITKEYFOLDER} to ${DEST}${NETNAME}/hosts/"
  rm -R ${DEST}${NETNAME}/hosts/
  ln -s ${GITDEST}${GITKEYFOLDER} ${DEST}${NETNAME}/hosts

fi
echo "done"
